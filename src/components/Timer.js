import React from 'react';
import PropTypes from 'prop-types';

export default function Timer ({
  hours,
  minutes,
  seconds
}) {
  return (
    <div>
      <span>{ hours < 10 ? `0${hours}` : `${hours}` }</span>
      :
      <span>{ minutes < 10 ? `0${minutes}` : `${minutes}` }</span>
      :
      <span>{ seconds < 10 ? `0${seconds}` : `${seconds}` }</span>
    </div>
  );
}

Timer.propTypes = {
  hours: PropTypes.number,
  minutes: PropTypes.number,
  seconds: PropTypes.number,
}

Timer.defaultProps = {
  hours: 0,
  minutes: 0,
  seconds: 0,
};



