import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

import ActiveTimer from './ActiveTimer';

test('render without localStorage', () => {
  const wrapper = shallow(
    <ActiveTimer
      miliseconds={3600000}
    />
  );
  expect(wrapper.find('Timer').props().hours).toEqual(1);
});

test('render with localStorage', () => {
  delete global.localStorage;
  global.localStorage = {
    getItem: () =>  7200000,
  };
  const wrapper = shallow(
    <ActiveTimer
      miliseconds={3600000}
    />
  );
  expect(wrapper.find('Timer').props().hours).toEqual(2);
});

